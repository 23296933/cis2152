<?php

class Node {
  public function __construct($id, $label, $children) {
    $this->id = $id;
    $this->label = $label;
    $this->children = $children;
  }
}

$tree = new Node(87, 'A', [
  new Node(18, 'B', [
    new Node(1, 'C', []),
    new Node(2, 'D', [])
  ]),
  new Node(43, 'E', [
    new Node(23, 'F', []),
  new Node(42, 'G', [])
  ])
]);

function print_tree($node, $indent="") {
  print($indent . $node->id . ": " . $node->label . "\n");
  for($idx = 0; $idx < count($node->children); $idx++)  {
    print_tree($node->children[$idx], $indent . "  ");
  }
}

print_tree($tree);
