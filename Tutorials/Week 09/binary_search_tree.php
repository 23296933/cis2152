<?php

class Node {
  public function __construct($id, $label, $left, $right) {
    $this->id = $id;
    $this->label = $label;
    $this->left = $left;
    $this->right = $right;
  }
}

function print_tree($node, $indent="") {
  if($node == null) {
    print($indent . "null\n");
  } else {
    print($indent . $node->id . ": " . $node->label . "\n");
    print_tree($node->left, $indent . "  ");
    print_tree($node->right, $indent . "  ");
  }
}
print_tree($tree);

function insert_node($node, $new_node) {
  if($new_node->id < $node->id) {
    if($node->left == null) {
      $node->left = $new_node;
    } else {
      insert_node($node->left, $new_node);
    }
  } else if($new_node->id > $node->id) {
    if(node->right == null) {
      $node->right = $new_node;
  } else {
    insert_node($node->right, $new_node);
  }
  }
}
