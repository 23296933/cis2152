function Node (id, label, children) {
  this.id = id;
  this.label = label;
  this.children = children;
}

var tree = new Node(87, 'A', [
  new Node(18, 'B', [
    new Node(1, 'C', []),
    new Node(2, 'D', [])
  ]),
  new Node(43, 'E', [
    new Node(23, 'F', []),
  new Node(42, 'G', [])
  ])
]);

function print_tree(node, indent) {
  if(indent === undefined) {
    indent = '';
  }
  console.log(indent + node.id + ": " + node.label);
  for(var idx = 0; idx < node.children.length; idx++)  {
    print_tree(node.children[idx], indent + '  ');
  }
}

function depth_first(needle, node) {
  console.log("Visiting Node " + node.label);
  if(needle == node.id) {
    return node;
  } else {
    for(var idx = 0; idx < node.children.length; idx++) {
      var result = depth_first(needle, node.children[idx]);
      if(result !== null) {
        return result;
      }
    }
    return null;
  }
}
print_tree(tree);
console.log("Found id 42 at " + depth_first(42, tree).label);
