function Node (id, label, children) {
  this.id = id;
  this.label = label;
  this.children = children;
}

var tree = new Node(87, 'A', [
  new Node(18, 'B', [
    new Node(1, 'C', []),
    new Node(2, 'D', [])
  ]),
  new Node(43, 'E', [
    new Node(23, 'F', []),
  new Node(42, 'G', [])
  ])
]);

function print_tree(node, indent) {
  if(indent === undefined) {
    indent = '';
  }
  console.log(indent + node.id + ": " + node.label);
  for(var idx = 0; idx < node.children.length; idx++)  {
    print_tree(node.children[idx], indent + '  ');
  }
}

function depth_first(needle, node, limit) {
  if(limit > 0) {
    console.log("Visiting Node " + node.label);
    if(needle == node.id) {
      return node;
    } else {
      for(var idx = 0; idx < node.children.length; idx++) {
        var result = depth_first(needle, node.children[idx], limit - 1);
        if(result !== null) {
          return result;
        }
      }
      return null;
    }
  } else {
    return null;
  }
}
function tree_depth(node) {
  var max_depth = 0;
  for(var idx = 0; idx < node.children.length; idx++) {
    max_depth = Math.max(max_depth, tree_depth(node.children[idx]));
  }
  return max_depth + 1;
}
console.log(tree_depth(tree));
print_tree(tree);

function iterative_deepening(needle, tree) {
  var max_depth = tree_depth(tree);
  for(var depth = 1; depth <= max_depth; depth++) {
    var result = depth_first(needle, tree, depth);
    if(result !== null) {
      return result;
    }
  }
  return null;
}

console.log("Found id 42 at " + iterative_deepening(42, tree).label);
