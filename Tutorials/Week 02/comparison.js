var age = 64;

if (age < 64) {
  console.log('You are too young for a free bus pass');
}
if (age == 64) {
  console.log('You can have your free buss pass next year');
}
if (age > 64) {
  console.log('You should already have your free bus pass');
}

var password = 'secret';
if (password == 'secret') {
  console.log('You may enter');
}

var name1 = 'Mark';
var name2 = 'Dave';

if (name1 > name2) {
  console.log('Mark comes after Dave (alphabetically)');
}
