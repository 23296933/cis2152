<?php

$age = 64;

if ($age < 64) {
  print('You are too young for a free bus pass');
}
if ($age == 64) {
  print('You can have your free buss pass next year');
}
if ($age > 64) {
  print('You should already have your free bus pass');
}

$password = 'secret';
if ($password == 'secret') {
  print('You may enter');
}

$name1 = 'Mark';
$name2 = 'Dave';

if ($name1 > $name2) {
  print('Mark comes after Dave (alphabetically)');
}
