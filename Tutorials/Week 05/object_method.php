<?php

class Person {
  public $name = '';
  public $age = '';

  function sayHello() {
    return 'Hello ' . $this->name;
  }

  function canDrink() {
    if ($this->age >= 18) {
      return true;
    } else {
      return false;
    }
  }
}
$mark = new Person();
$dave = new Person();
$dave->name = 'Dave';
$dave->age = '17';
print($dave->sayHello() . "\n");
print($dave->canDrink() . "\n");
$mark->name = 'Mark';
print($mark->sayHello() . "\n");
