function Person() {
  this.name = '';
  this.age = '';
}
Person.prototype.sayHello = function() {
  return 'Hello ' + this.name;
};
Person.prototype.canDrink = function() {
  if (this.age >= 18) {
    return true
  } else {
    return false
  }
}

var mark = new Person();
var dave = new Person();
dave.name = 'Dave';
dave.age = '17';
console.log(dave.canDrink());
console.log(dave.sayHello());
mark.name = 'Mark';
console.log(mark.sayHello());
