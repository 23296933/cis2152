Array.prototype.peek = function() {
  return this[this.length - 1];
}

var list = [];
list.push('a');
console.log(list);
list.push('b');
console.log(list);
console.log(list.pop());
console.log(list);
list.push('b');
console.log(list.peek());
console.log(list);
