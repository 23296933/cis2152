<?php

class Person {
  public $name = '';

  public function __construct($name) {
    $this->name = $name;
  }
  public function sayHello() {
    return 'Hello ' . $this->name;
  }
}

$mark = new Person('Mark');
print($mark->sayHello() . "\n");

class Student extends Person {
  public $snr = 0;

  public function __construct($name, $snr) {
    parent::__construct($name);
    $this->snr = $snr;
  }

  public function register() {
    return 'Register ' . $this->name . ' with number ' . $this->snr;
  }
}

$dave = new Student('Dave', 123456);
print($dave->sayHello() . "\n");
print($dave->register() . "\n");
