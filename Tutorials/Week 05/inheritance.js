function Person(name) {
  this.name = name;
}

Person.prototype.sayHello = function() {
  return 'Hello ' + this.name;
};

var mark = new Person('Mark');
console.log(mark.sayHello());

function Student(name, snr) {
  Person.call(this, name);
  this.snr = snr;
}

Student.prototype = new Person();
Student.prototype.constructor = Student;

Student.prototype.register = function() {
  return 'Register ' + this.name + ' with number ' + this.snr;
}

var dave = new Student('Dave', 123456);
console.log(dave.sayHello());
console.log(dave.register());
