function find(needle, haystack) {
  if(haystack.length > 0) {
    if(needle == haystack[0]) {
      return 0;
    } else {
      var idx = find(needle, haystack.slice(1));
      if(idx !== null) {
        idx = idx + 1;
      }
      return idx;
    }
  } else {
    return null;
  }
}

var list = [38, 93, 23, 48, 32, 82, 45];
console.log(find(23, list));
console.log(find(38, list));
console.log(find(13, list));
