<?php

function find($needle, $haystack) {
  for($idx = 0; $idx < count($haystack); $idx++) {
    if($needle == $haystack[$idx]) {
      return $idx;
    }
  }
  return null;
}
$list = [38, 93, 23, 48, 32, 82, 45];
print(find(23, $list) . "\n");
print(find(38, $list) . "\n");
print(find(13, $list) . "\n");
