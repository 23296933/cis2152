<?php

function find($needle, $haystack) {
  if(count($haystack) == 0) {
    return null;
  } else {
    $middle = floor(count($haystack) / 2);
    if($needle == $haystack[$middle]) {
      return $middle;
    } else if($needle < $haystack[$middle]) {
      return find($needle, array_slice($haystack, 0, $middle));
    } else {
      return $middle + 1 + find($needle, array_slice($haystack, $middle + 1));
    }
  }
}

$list = [23, 32, 38, 45, 82, 93];
print(find(23, $list) . "\n");
print(find(38, $list) . "\n");
print(find(82, $list) . "\n");
print(find(13, $list) . "\n");
