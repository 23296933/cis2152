function find(needle, haystack) {
  var left = 0;
  var right = haystack.length - 1;
  while(left <= right) {
    var middle = Math.floor(left + (right - left) / 2);
    if(needle == haystack[middle]) {
      return middle;
    } else if(haystack[middle] < needle) {
      left = middle + 1;
    } else {
      right = middle - 1;
    }
  }
  return null;
}

var list = [23, 32, 38, 45, 82, 93];
console.log(find(23, list));
console.log(find(38, list));
console.log(find(82, list));
console.log(find(13, list));
