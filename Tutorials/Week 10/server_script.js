var http = require('http');
var url = require('url');

var server = http.createServer(function(request, response) {
  var data = url.parse(request.url, true);
  response.write('<html><head><title>Test</title></head>');
  response.write('<body>');
  if(data['query'] && data['query']['name']) {
    response.write('<p>Hello ' + data.query.name + '</p>');
  }
  response.write('<form>');
  response.write('<input type="text" name="name"/><input type="submit" value="Say Hello!"/>');
  response.write('</form>');
  response.write('</body></html>');
  response.end();
});

server.listen(8000);
