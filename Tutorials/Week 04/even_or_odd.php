<?php

function is_even($n) {
  if($n == 0) {
    return true;
  } else {
    return is_odd($n - 1);
  }
}
function is_odd($n) {
  if($n == 0) {
    return false;
  } else {
    return is_even($n -1);
  }
}
print(is_even(0) . "\n");
print(is_even(4) . "\n");
print(is_even(1) . "\n");
print(is_odd(1) . "\n");
print(is_odd(4) . "\n");
