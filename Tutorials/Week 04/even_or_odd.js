function is_even(n) {
  if(n == 0) {
    return true;
  } else {
    return is_odd(n - 1);
  }
}
function is_odd(n) {
  if(n == 0) {
    return false;
  } else {
    return is_even(n - 1);
  }
}
console.log(is_even(0));
console.log(is_even(4));
console.log(is_even(1));
console.log(is_odd(1));
console.log(is_odd(4));
