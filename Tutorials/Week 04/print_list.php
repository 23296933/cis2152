<?php

function print_list($list) {
  print($list[0] . "\n");
  if(count($list) > 1) {
    print_list(array_slice($list, 1));
  }
}

print_list(['a', 'b', 'c', 'r', 1, 'e']);
