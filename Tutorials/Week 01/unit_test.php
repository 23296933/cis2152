<?php
class ArithmeticTest extends PHPUnit_Framework_TestCase {
  public function testAddiction() {
    $a = 42;
    $b = 5;
    $this->assertEquals(47, $a + $b);
  }
}
