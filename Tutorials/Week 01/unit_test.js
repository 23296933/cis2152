var assert = require('assert');
describe('Arithmetic tests', function() {
  it ('Addiction should calculate the sum, of two values', function() {
    var a = 42;
    var b = 5;
    assert.equal (a + b, 47);
  });
});
