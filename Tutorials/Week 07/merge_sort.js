function merge(a, b) {
  var steps = steps + 1;
  if(a.length > 0 && b.length > 0) {
    if(a[0] <= b[0]) {
      var result = [a[0]];
      var sorted = merge(a.slice(1), b);
      result = result.concat(sorted);
      return result;
    } else {
        var result = [b[0]];
        var sorted = merge(a, b.slice(1));
        result = result.concat(sorted);
        return result;
    }
  } else if(a.length > 0 && b.length == 0) {
      return a;
  } else if(a.length == 0 && b.length > 0) {
      return b;
  }
}

function merge_sort(list) {
  if(list.length > 1) {
    var split_point = list.length / 2;
    var part1 = list.slice(0, split_point);
    var part2 = list.slice(split_point);
    var sorted1 = merge_sort(part1);
    var sorted2 = merge_sort(part2);
    return merge(sorted1, sorted2);
  } else {
      return list;
  }
}

var items = [327, 1, 32, 13, 93, 23];
console.log(items);
var sorted = merge_sort(items.slice());
console.log(sorted);
