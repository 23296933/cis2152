<?php

function insertion_sort($items) {
  global $steps;

  for($idx = 1; $idx < count($items); $idx++) {
    $idx2 = $idx;
    $steps = $steps + 1;
    while($idx2 > 0 && $items[$idx2 - 1] > $items[$idx2] ) {
      $tmp = $items[$idx2 - 1];
      $items[$idx2 - 1] = $items[$idx2];
      $items[$idx2] = $tmp;
      $idx2 = $idx2 - 1;
      $steps = $steps + 1;
    }
  }
  return $items;
}

function merge($a, $b) {
  global $steps;
  $teps = $steps + 1;
  if(count($a) > 0 && count($b) > 0) {
    if($a[0] <= $b[0]) {
      return array_merge([$a[0]], merge(array_slice($a, 1), $b));
    } else {
        return array_merge([$b[0]], merge($a, array_slice($b, 1)));;
    }
  } else if(count($a) > 0) {
      return $a;
  } else if(count($b) > 0) {
      return $b;
  }
}

function merge_sort($items) {
  if(count($items) > 1) {
    $split_point = count($items) / 2;
    $part1 = array.slice($items, 0, $split_point);
    $part2 = array.slice($items, $split_point);
    $sorted1 = merge_sort($part1);
    $sorted2 = merge_sort($part2);
    return merge($sorted1, $sorted2);
  } else {
      return $items;
  }
}

$items = [];
for($idx = 0; $idx < 1000; $idx++) {
  $items[] = rand(0, 10000);
}
$steps = 0;
insertion_sort($items);
print('Random Insertion Sort: ' . $steps . " steps\n");
$steps = 0;
merge_sort($items);
print('Random Merge Sort: ' . $steps . " steps\n");
