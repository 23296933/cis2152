<?php

$steps = 0;

function insertion_sort($list) {
  global $steps;

  for($idx = 1; $idx < count($list); $idx++) {
    $idx2 = $idx;
    $steps = $steps + 1;
    while($idx2 > 0 && $list[$idx2 - 1] > $list[$idx2] ) {
      $tmp = $list[$idx2 - 1];
      $list[$idx2 - 1] = $list[$idx2];
      $list[$idx2] = $tmp;
      $idx2 = $idx2 - 1;
      $steps = $steps + 1;
    }
  }
  return $list;
}

$items = [372, 1, 32, 93, 23];
print_r($items);
$sorted = insertion_sort($items);
print_r($sorted);
