var steps = 0;

function insertion_sort(list) {
  for(var idx = 1; idx < list.length; idx++) {
    var idx2 = idx;
    while(idx2 > 0 && list[idx2 - 1] > list[idx2] ) {
      var tmp = list[idx2 - 1];
      list[idx2 - 1] = list[idx2];
      list[idx2] = tmp;
      idx2 = idx2 - 1;
      steps = steps + 1;
    }
    steps = steps + 1;
  }
  return list;
}

var items = [372, 1, 32, 93, 23];
console.log(items);
var sorted = insertion_sort(items.slice());
console.log(sorted);
