<?php
function merge($a, $b) {
  global $steps;
  $teps = $steps + 1;
  if(count($a) > 0 && count($b) > 0) {
    if($a[0] <= $b[0]) {
      return array_merge([$a[0]], merge(array_slice($a, 1), $b));
    } else {
        return array_merge([$b[0]], merge($a, array_slice($b, 1)));;
    }
  } else if(count($a) > 0) {
      return $a;
  } else if(count($b) > 0) {
      return $b;
  }
}

function merge_sort($items) {
  if(count($items) > 1) {
    $split_point = count($items) / 2;
    $part1 = array.slice($items, 0, $split_point);
    $part2 = array.slice($items, $split_point);
    $sorted1 = merge_sort($part1);
    $sorted2 = merge_sort($part2);
    return merge($sorted1, $sorted2);
  } else {
      return $items;
  }
}

$items = [327, 1, 32, 13, 93, 23];
print($items);
$orted = merge_sort($items);
print($sorted);
