var a = 4;
function visible(value) {
  console.log('Visible (parameter value): ' + value);
  console.log('Visible (variable a): ' + a);
}

function param_hidden(a) {
  console.log('Param_hidden (parameter a): ' + a);
  console.log('Param_hidden (variable a): ' + a);
}

function hidden(value) {
  var a = 32;
  console.log('Hidden (parameter value): '  + value);
  console.log('Hidden (variable a): ' + a);
}

visible(a);
param_hidden(a);
hidden(a);
hidden(12);
