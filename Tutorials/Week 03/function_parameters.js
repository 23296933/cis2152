readline = require('readline-sync');
function main_menu(options) {
  console.log("+===========+");
  console.log("| Main Menu |");
  console.log("+===========+");

  for (var idx = 0; idx < options.length; idx++) {
    console.log(idx + ': ' + options[idx]);
  }
  var choice = readline.question('Choice: ');
  console.log('You chose option ' + choice);
}

main_menu(['Function 1', 'Function 2', 'Function 3']);
main_menu(['Function 5', 'Function 6', 'Function 7']);
