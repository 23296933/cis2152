<?php
function main_menu($options) {
  print("+===========+\n");
  print("| Main Menu |\n");
  print("+===========+\n");

  for ($idx = 0; $idx < count($options); $idx++) {
    print($idx . ': ' . $options[$idx] . "\n");
  }

  $choice = readline('Choice: ');
  print('You chose option ' . $choice);
}

main_menu(['Function 1', 'Function 2', 'Function 3']);
main_menu(['Function 5', 'Function 6', 'Function 7']);
