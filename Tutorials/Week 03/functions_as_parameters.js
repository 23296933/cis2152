var add = function(a, b) {
  return a + b;
}

var sub = function(a, b) {
  return a - b;
}

function math(a, op, b) {
  return op(a, b);
}

console.log(math(4, add, 12));
console.log(math(4, sub, 12));

var mult = function(a, b) {
  return a * b;
}

console.log(math(4, mult, 12));
