<?php
$a = 4;
function visible($value) {
  global $a;
  print('Visible (parameter $value): ' . $value . "\n");
  print('Visible (variable $a): ' . $a . "\n");
}

function param_hidden($a) {
  print('Param_hidden (parameter $a): ' . $a . "\n");
  print('Param_hidden (variable $a): ' . $a . "\n");
}

function hidden($value) {
  $a = 32;
  print('Hidden (parameter $value): '  . $value . "\n");
  print('Hidden (variable $a): ' . $a . "\n");
}

visible($a);
param_hidden($a);
param_hidden(16);
hidden($a);
hidden(12);
