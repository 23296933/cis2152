<?php
//View list of products(name & price)
//Variable for the list of products
$product = array();
//Hash Table of products' codes, names and prices
//Each product has a different code
$product['A1'] = 'Dairy Milk Chocolate, £1';
$product['A2'] = 'KitKat Chocolate, £1';
$product['A3'] = 'Ready Salted Crisp, £1.50';
$product['B1'] = 'Haribo Sweets, £1.20';
$product['B2'] = 'Jaffa Cake, £2';
$product['B3'] = 'Cheese and Onion Crisp, £1.50';
$product['C1'] = 'Pepsi, £1.90';
$product['C2'] = 'Water, £1.60';
$product['C3'] = 'Coca-Cola, £1.90';
//Print the whole table to let the user view all the products and their prices
print_r($product);

//Variable for credit
$credit = 0;
//Credit is shown to let the user know that there is no credit at the start
print('Credit: £' . $credit .  "\n");

//Add credit
//Ask the user to enter an amount credit
$add_credit = readline('How Much Credit Do You Want to Add: £');
//The added credit will added on the current credit
$credit = $credit + $add_credit;
//This will display the  amount of credit to the user
print('Credit: £' . $credit .  "\n");

//Purchasing a product(s)
//Instructions to help the user
Print('Enter a Product Code to purchase Product' . "\n");
Print('Enter a "R" to get a refund of your credit' . "\n");
//This loop will go on until there is no more credit
while ($credit > 0.00) {
  //This will ask the user to enter a product code
  $purchase_product = readline('Enter Code: ');
  //When a product is purchased, the credit will get subtracted from the price of the product
  //After purchase, the amount of credit will be displayed to the user
  if ($purchase_product == 'A1') {
    print($product['A1'] . "\n");
    $credit = $credit - 1;
    print('Credit: £' . $credit .  "\n");
  } else if ($purchase_product == 'A2') {
    print($product['A2'] . "\n");
    $credit = $credit - 1;
    print('Credit: £' . $credit .  "\n");
  } else if ($purchase_product == 'A3') {
    print($product['A3'] . "\n");
    $credit = $credit - 1.50;
    print('Credit: £' . $credit .  "\n");
  } else if ($purchase_product == 'B1') {
    print($product['B1'] . "\n");
    $credit = $credit - 1.20;
    print('Credit: £' . $credit .  "\n");
  } else if ($purchase_product == 'B2') {
    print($product['B2'] . "\n");
    $credit = $credit - 2;
    print('Credit: £' . $credit .  "\n");
  } else if ($purchase_product == 'B3') {
    print($product['B3'] . "\n");
    $credit = $credit - 1.50;
    print('Credit: £' . $credit .  "\n");
  } else if ($purchase_product == 'C1') {
    print($product['C1'] . "\n");
    $credit = $credit - 1.90;
    print('Credit: £' . $credit .  "\n");
  } else if ($purchase_product == 'C2') {
    print($product['C2'] . "\n");
    $credit = $credit - 1.60;
    print('Credit: £' . $credit .  "\n");
  } else if ($purchase_product == 'C3') {
    print($product['C3'] . "\n");
    $credit = $credit - 1.90;
    print('Credit: £' . $credit .  "\n");
    //Ask for a refund of their credit
  } else if ($purchase_product == 'R') {
    //A message for the user to show how much credit has returned
    print('Your Refund Is £'. $credit . ' Thank You, Come Again' . "\n");
    //When credit is returned, the current credit becomes zero and loop ends
    $credit = 0;
  } else {
    //If an incorrect code is entered, an error message will be shown
    print('Invalid Code Entered' . "\n");
  }
  //When there is no more credit, credit is zero, A message to say thank you
  //After message, loop ends
  if ($credit == 0) {
    print('Thank You, Come Again' . "\n");
  }
  //When the user does not have enough credit, credit is less than zero, an error message will shown to the user
  //After message, loop ends
  if ($credit < 0) {
    print('You Do Not Have Enough Credit To Buy This Product. Purchase Cancelled' . "\n");
  }
}
