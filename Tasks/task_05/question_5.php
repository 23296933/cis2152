<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
// EndStudentCode

class Question5Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $bmw = new Car('BMW', 'Z2');
        $this->assertEquals('BMW', $bmw->make);
        $this->assertEquals('Z2', $bmw->model);
        $this->assertEquals(4, $bmw->wheels);
        $this->assertEquals('BMW Z2 (4 wheels)', $bmw->summary());
        $this->assertEquals('Vroooooom', $bmw->start());
        $ktm = new Bike('KTM', '1050 Adventure');
        $this->assertEquals('KTM', $ktm->make);
        $this->assertEquals('1050 Adventure', $ktm->model);
        $this->assertEquals(2, $ktm->wheels);
        $this->assertEquals('KTM 1050 Adventure (2 wheels)', $ktm->summary());
        $this->assertEquals('Vroooooom', $ktm->start());
    }
}

