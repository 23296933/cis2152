// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function reverse(list) {
    var result = [];
    for(var idx = list.length - 1; idx >= 0; idx--) {
        result.push(list[idx]);
    }
    return result;
}
// EndStudentCode

var assert = require('assert');
describe('Question 4', function() {
    it('test', function() {
        assert.deepEqual(reverse([3, 4]), [4, 3]);
    });
});
