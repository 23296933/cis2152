<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function find($needle, $haystack) {
  $left = 0;
  $right = count($haystack) - 1;
  while($left <= $right) {
    $middle = floor($left + ($right - $left) / 2);
    if($needle == $haystack[$middle]) {
      return $middle;
    } else if($haystack[$middle] < $needle) {
      $left = $middle + 1;
    } else {
      $right = $middle - 1;
    }
  }
  return null;
}
// EndStudentCode

class Question2Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $list = [83, 29, 38, 93, 12, 63];
        $this->assertEquals(0, find(83, $list));
        $this->assertEquals(5, find(63, $list));
        $this->assertEquals(null, find(23, $list));
        $this->assertEquals(2, find(38, $list));
    }
}
