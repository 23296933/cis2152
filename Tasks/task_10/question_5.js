// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function common_length(a, b) {

}
// EndStudentCode

var assert = require('assert');
describe('Question 5', function() {
    it('test', function() {
        assert.equal(common_length('Hello', 'Bye'), 0);
        assert.equal(common_length('Hello', 'Hello World'), 5);
        assert.equal(common_length('Heap', 'Hello'), 2);
        assert.equal(common_length('This is it', 'This is it'), 10);
    });
});
