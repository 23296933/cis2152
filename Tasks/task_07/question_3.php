<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
$steps = 0;

function string_insertion($list) {
  global $steps;

  for($idx = 1; $idx < count($list); $idx++) {
    $idx2 = $idx;
    $steps = $steps + 1;
    while($idx2 > 0 && strlen($list[$idx2 - 1]) > strlen($list[$idx2])) {
      $tmp = $list[$idx2 - 1];
      $list[$idx2 - 1] = $list[$idx2];
      $list[$idx2] = $tmp;
      $idx2 = $idx2 - 1;
      $steps = $steps + 1;
    }
  }
  return $list;
}
// EndStudentCode

class Question3Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $items = ['Mark', 'Dave', 'Tom', 'Robert'];
        $sorted = string_insertion($items);
        $this->assertEquals(['Tom', 'Mark', 'Dave', 'Robert'], $sorted);
        $items = ['aabb', 'aa', 'aba', 'b'];
        $sorted = string_insertion($items);
        $this->assertEquals(['b', 'aa', 'aba', 'aabb'], $sorted);
    }
}
