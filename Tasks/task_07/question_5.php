<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
$steps = 0;

function student_sort($list) {
  global $steps;

  for($idx = 1; $idx < count($list); $idx++) {
    $idx2 = $idx;
    $steps = $steps + 1;
    while($idx2 > 0 && $list[$idx2 - 1] > $list[$idx2] ) {
      $tmp = $list[$idx2 - 1];
      $list[$idx2 - 1] = $list[$idx2];
      $list[$idx2] = $tmp;
      $idx2 = $idx2 - 1;
      $steps = $steps + 1;
    }
  }
  return $list;
}

// EndStudentCode

class Student {
    function __construct($name, $snr) {
        $this->name = $name;
        $this->snr = $snr;
    }
}

class Question5Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $mark = new Student('Mark', 9947832);
        $dave = new Student('Dave', 483373);
        $chris = new Student('Chris', 6482724);
        $students = [$mark, $dave, $chris];
        $sorted = student_sort($students);
        $this->assertEquals([$dave, $chris, $mark], $sorted);
    }
}
